
package root.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/")
public class validaedad {
    @GET
    @Path("/edad/{edad: [0-9]+}")
    public String getMayorEdad(@PathParam("edad") String edad){
        int intedad=Integer.parseInt(edad);
        String res="";
        if (intedad>=18){
            res= "Usted es mayor de edad";
        }else{
            res= "Usted es menor de edad";
        }
        return res;
    }
}
